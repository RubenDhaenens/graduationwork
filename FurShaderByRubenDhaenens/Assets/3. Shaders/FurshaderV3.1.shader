﻿Shader "Fur/FurshaderV3.1"{
	Properties{
		_DiffuseTex("Diffuse Texture", 2D) = "White"{}
		//Normal
		_NormalTex("Normal Texture", 2D) = "Bump"{}
		_NormalIntensity("Normal Intensity", range(0,1)) = 1.0
		// Shells
		_NoiseTex("Noise Texture", 2D) = "Red"{}
		_MaskTex("Mask Texture", 2D) = "Red"{}
		_ShellMinValue("Shell MinValue", range(0,1)) = 0.9
		_ShellMaxValue("Shell MaxValue", range(0,1)) = 0.9
		// Length
		_LengthTex("Length Texture", 2D) = "Red"{}
		_FurLength("Fur Length", range(0,1)) = 1.0
		_ILayerAmount("Amount of layers", range(5, 32)) = 32
		// Gravity
		_Gravity("Gravity", range(-1, 0)) = -0.02
		//Light
		_SpecColor("Specular color", Color) = (1.0,0.0,0.0,1.0)
		_Shininess("Shininess", range(0,1)) = 0.0
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 100
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			uniform sampler2D _DiffuseTex;
			uniform sampler2D _NormalTex;
			uniform float4 _SpecColor;
			uniform float4 _RimColor;
			uniform float _Shininess;
			uniform float _RimPower;
			uniform float _NormalIntensity;

			struct appdata
			{
				float4 pos : POSITION;
				float3 normal :NORMAL;
				float2 tex : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;			//4
				float2 tex : TEXCOORD0;				//6
				float4 posWorld : TEXCOORD1;		//10
				float3 normalWorld : TEXCOORD2;		//13
				float3 tangentWorld : TEXCOORD3;	//16
				float3 binormalWorld : TEXCOORD4;	//19
			};

			// Main vertex shader
			v2f vert(appdata i)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(i.pos);
				o.tex = i.tex;
				o.posWorld = mul(unity_ObjectToWorld, i.pos);
				o.normalWorld = normalize(mul(fixed4(i.normal, 0), unity_WorldToObject));
				o.tangentWorld = normalize(mul(unity_ObjectToWorld, i.tangent));
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * i.tangent.w);
				return o;
			}

			//Diffuse
			float3 CalculateDiffuse(float3 normal, float2 texCoord)
			{
				float3 diffuseColor = tex2D(_DiffuseTex, texCoord).xyz;
				float diffuseIntensity = dot(-normal, _WorldSpaceLightPos0.xyz);
				diffuseIntensity = saturate(diffuseIntensity);

				diffuseColor *= diffuseIntensity;
				return diffuseColor;
			}

			//Specular
			float3 CalculateSpecular(float3 viewDirection, float3 normal, float2 texCoord)
			{
				//IF USE specular / phong / ...
				float3 reflection = reflect(_WorldSpaceLightPos0, normal);
				float3 specularColor = pow(saturate(dot(-viewDirection, reflection)), _Shininess) * _SpecColor;
				return specularColor;
			}

			//Normal mapping function
			float3 CalculateNormal(float3 tangent, float3 normal, float2 texCoord)
			{
				//IF USE normal
				float3 binormal = cross(normal, tangent);
				binormal = -binormal;
				float3x3 localAxis = float3x3(tangent, binormal, normal);
				float4 texN = tex2D(_NormalTex, texCoord);
				float3 sampleNormal = _NormalIntensity * 2 * texN.rgb;
				float3 newNormal = mul(sampleNormal, localAxis);
				return newNormal;
			}

			// Main fragment shader
			fixed4 frag(v2f i) : COLOR
			{
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 lightDirection;
				
				//NORMAL
				float3 newNormal = CalculateNormal(i.tangentWorld, i.normalWorld, i.tex);
				
				//SPECULAR
				float3 specColor = CalculateSpecular(viewDirection, newNormal, i.tex);

				//DIFFUSE
				float3 diffColor = CalculateDiffuse(newNormal, i.tex);

				//FINAL
				float3 finalColor = diffColor + specColor;


				////normal
				//float3 localCoords = float3(2.0 * texN.ag - float2(1.0, 1.0), 1.0);
				//float3x3 local2WorldTranspose = float3x3(
				//	i.tangentWorld,
				//	i.binormalWorld,
				//	i.normalWorld
				//);
				//float normalDirection = normalize(mul(localCoords, local2WorldTranspose));

				return fixed4(finalColor, 1.0);
			}
			ENDCG
		}
		
		//Pass
		//{
		//	CGPROGRAM
		//	#pragma vertex vert
		//	#pragma geometry geom
		//	#pragma fragment frag
		//	
		//	#include "UnityCG.cginc"
		//
		//	struct v2g
		//	{
		//		float4 pos : POSITION;
		//		float3 norm : NORMAL;
		//		float2 tex : TEXCOORD0;
		//	};
		//
		//	struct g2f
		//	{
		//		float4 pos : SV_POSITION;
		//		float3 norm : NORMAL;
		//		float2 tex : TEXCOORD0;
		//		float lay : LAYER;
		//	};
		//
		//	sampler2D _DiffuseTex;
		//	fixed4 _DiffuseTex_ST;
		//	sampler2D _NoiseTex;
		//	fixed4 _NoiseTex_ST;
		//	sampler2D _MaskTex;
		//	fixed4 _MaskTex_ST;
		//	sampler2D _LengthTex;
		//	fixed4 _LengthTex_ST;
		//
		//	fixed _ShellMinValue;
		//	fixed _ShellMaxValue;
		//	fixed _FurLength;
		//	uint _ILayerAmount;
		//	fixed _Gravity;
		//
		//	void CreateVertex(inout TriangleStream<g2f> triStream, fixed4 vertexposition, fixed3 normal, fixed2 texCoord, fixed layerIndex)
		//	{
		//		g2f data;
		//		//data.pos = mul(float4(vertexposition.rgb, 1.0f), unity_ObjectToWorld);
		//		data.pos = UnityObjectToClipPos(vertexposition);
		//
		//		data.norm = normalize(mul(fixed4(normal.xyz, 0), unity_ObjectToWorld));
		//
		//		data.tex = texCoord;
		//		data.lay = layerIndex;
		//		triStream.Append(data);
		//	}
		//
		//	v2g vert(v2g i)
		//	{
		//		v2g o;
		//		o.pos = i.pos;
		//		o.tex = TRANSFORM_TEX(i.tex, _DiffuseTex);
		//		o.norm = normalize(i.norm);
		//		return o;
		//	}
		//
		//	[MaxVertexCount(96)]
		//	void geom(triangle v2g v[3], inout TriangleStream<g2f> triStream)
		//	{
		//		float LOD = 0;
		//		float layerAmount = 1.0 / _ILayerAmount;
		//		float2 texLength0 = TRANSFORM_TEX(v[0].tex, _LengthTex);
		//		float2 texLength1 = TRANSFORM_TEX(v[1].tex, _LengthTex);
		//		float2 texLength2 = TRANSFORM_TEX(v[2].tex, _LengthTex);
		//
		//		float furLength0 = tex2Dlod(_LengthTex, float4(texLength0, 0, LOD)).r * _FurLength * 0.001;
		//		float furLength1 = tex2Dlod(_LengthTex, float4(texLength1, 0, LOD)).r * _FurLength * 0.001;
		//		float furLength2 = tex2Dlod(_LengthTex, float4(texLength2, 0, LOD)).r * _FurLength * 0.001;
		//
		//		float offset0 = furLength0 * layerAmount;
		//		float offset1 = furLength1 * layerAmount;
		//		float offset2 = furLength2 * layerAmount;
		//
		//		float furLength = (furLength0 + furLength1 + furLength2) / 3;
		//
		//		int strongHair = _ILayerAmount / 4;
		//
		//		float4 forces = float4(/*-cos(_SinTime.w*6.2831)*0.002*/0.0, _Gravity, -(cos(_Time.w)*sin(_Time.w * 0.5))*0.5, 0.0);
		//		forces *= furLength;
		//
		//		for (uint i = 0; i < _ILayerAmount; ++i)
		//		{
		//			v[0].pos += float4(float3(v[0].norm), 0.0) * offset0;
		//			v[1].pos += float4(float3(v[1].norm), 0.0) * offset1;
		//			v[2].pos += float4(float3(v[2].norm), 0.0) * offset2;
		//
		//			int lay = i - strongHair;
		//			if (lay < 1)
		//			{
		//				lay = 0;
		//			}
		//
		//			CreateVertex(triStream, v[0].pos + (forces * lay / _ILayerAmount), v[0].norm, v[0].tex, i + 1);
		//			CreateVertex(triStream, v[1].pos + (forces * lay / _ILayerAmount), v[1].norm, v[1].tex, i + 1);
		//			CreateVertex(triStream, v[2].pos + (forces * lay / _ILayerAmount), v[2].norm, v[2].tex, i + 1);
		//
		//			triStream.RestartStrip();
		//		}
		//	}
		//
		//	fixed4 frag(g2f i) : SV_Target0
		//	{
		//		fixed2 texMask = TRANSFORM_TEX(i.tex, _MaskTex);
		//		fixed mask = tex2D(_MaskTex, texMask).r;
		//		if (mask < 0.5)
		//		{
		//			discard;
		//		}
		//
		//		fixed4 rgba = tex2D(_DiffuseTex, i.tex);
		//		fixed2 texNoise = TRANSFORM_TEX(i.tex, _NoiseTex);
		//		fixed shell = tex2D(_NoiseTex, texNoise).r;
		//		fixed alpha = _ShellMinValue + i.lay * ((_ShellMaxValue - _ShellMinValue) / _ILayerAmount);
		//		if (shell <= alpha)
		//		{
		//			discard;
		//		}
		//		rgba.a = alpha;
		//		return rgba;
		//	}
		//	ENDCG
		//}
	}
}