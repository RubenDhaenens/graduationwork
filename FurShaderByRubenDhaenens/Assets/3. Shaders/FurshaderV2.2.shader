﻿Shader "Fur/FurshaderV2.2"
{
	Properties
	{
		_DiffuseTex("Diffuse Texture", 2D) = "White"{}
		_NoiseTex("Noise Texture", 2D) = "Red"{}
		_ShellMinValue("Shell MinValue", range(0,1)) = 0.0
		_ShellMaxValue("Shell MaxValue", range(0,1)) = 1.0
		_FurLength("Fur Length", range(0,2)) = 0.5
		_ILayerAmount("Amount of layers", range(4, 32)) = 32
		_Gravity("Gravity", range(-1, 0)) = -0.65
		_LOD("LOD", float) = 100
		_WindDirection("Wind direction", Vector) = (1,0,1,0)
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			struct appdata
			{
				float4 pos : POSITION;
				float2 tex : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
			};

			sampler2D _DiffuseTex;

			v2f vert(appdata i)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(i.pos);
				o.tex = i.tex;
				return o;
			}
			fixed4 frag(v2f i) : SV_Target0
			{
				fixed4 col = tex2D(_DiffuseTex, i.tex);
				return col;
			}
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct v2g
			{
				float4 pos : POSITION;
				float3 norm : NORMAL;
				float2 tex : TEXCOORD0;
			};

			struct g2f
			{
				float4 pos : SV_POSITION;
				float3 norm : NORMAL;
				float2 tex : TEXCOORD0;
				float lay : LAYER;
			};

			sampler2D _DiffuseTex;
			sampler2D _NoiseTex;
			fixed4 _NoiseTex_ST;
			fixed3 _WindDirection;
			fixed _ShellMinValue;
			fixed _ShellMaxValue;
			fixed _FurLength;
			uint _ILayerAmount;
			fixed _Gravity;
			fixed LOD;

			void CreateVertex(inout TriangleStream<g2f> triStream, fixed4 vertexposition, fixed3 normal, fixed2 texCoord, fixed layerIndex)
			{
				g2f data;
				data.pos = UnityObjectToClipPos(vertexposition);

				data.norm = normalize(mul(fixed4(normal.xyz, 0), unity_ObjectToWorld));

				data.tex = texCoord;
				data.lay = layerIndex;
				triStream.Append(data);
			}

			v2g vert(v2g i)
			{
				v2g o;
				o.pos = i.pos;
				o.tex = i.tex;
				o.norm = normalize(i.norm);
				return o;
			}

			[MaxVertexCount(96)]
			void geom(triangle v2g v[3], inout TriangleStream<g2f> triStream)
			{
				float layerAmount = 1.0 / _ILayerAmount;

				float furLength0 = tex2Dlod(_DiffuseTex, float4(v[0].tex, 0, LOD)).a * _FurLength * 0.001;
				float furLength1 = tex2Dlod(_DiffuseTex, float4(v[1].tex, 0, LOD)).a * _FurLength * 0.001;
				float furLength2 = tex2Dlod(_DiffuseTex, float4(v[2].tex, 0, LOD)).a * _FurLength * 0.001;

				float offset0 = furLength0 * layerAmount;
				float offset1 = furLength1 * layerAmount;
				float offset2 = furLength2 * layerAmount;

				float furLength = (furLength0 + furLength1 + furLength2) / 3;

				int strongHair = _ILayerAmount / 4;
				float wave = (cos(_Time.w)*sin(_Time.w * 0.5))*0.5;
				_WindDirection.g = 0.0;
				float4 forces = float4(float3(-wave, _Gravity, wave)* _WindDirection.rgb, 0);
				forces *= furLength;

				for (uint i = 0; i < _ILayerAmount; ++i)
				{
					v[0].pos += float4(float3(v[0].norm), 0.0) * offset0;
					v[1].pos += float4(float3(v[1].norm), 0.0) * offset1;
					v[2].pos += float4(float3(v[2].norm), 0.0) * offset2;

					int lay = i - strongHair;
					if (lay < 1)
					{
						lay = 0;
					}

					float4 force = forces * lay / _ILayerAmount;

					CreateVertex(triStream, v[0].pos + force, v[0].norm, v[0].tex, i + 1);
					CreateVertex(triStream, v[1].pos + force, v[1].norm, v[1].tex, i + 1);
					CreateVertex(triStream, v[2].pos + force, v[2].norm, v[2].tex, i + 1);

					triStream.RestartStrip();
				}
			}

			fixed4 frag(g2f i) : SV_Target0
			{
				fixed mask = tex2D(_DiffuseTex, i.tex).a;
				if (mask == 0.0)
				{
					discard;
				}

				fixed4 rgba = tex2D(_DiffuseTex, i.tex);
				fixed2 texNoise = TRANSFORM_TEX(i.tex, _NoiseTex);
				fixed shell = tex2D(_NoiseTex, texNoise).r;
				fixed alpha = _ShellMinValue + i.lay * ((_ShellMaxValue - _ShellMinValue) / _ILayerAmount);
				if (shell <= alpha)
				{
					discard;
				}
				rgba.a = alpha;
				return rgba;
			}
			ENDCG
		}
	}
}
