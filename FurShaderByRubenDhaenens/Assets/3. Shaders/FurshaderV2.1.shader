﻿Shader "Fur/FurshaderV2.1"
{
	Properties
	{
		_DiffuseTex("Diffuse Texture", 2D) = "White"{}
		_NoiseTex("Noise Texture", 2D) = "Red"{}
		_ShellMinValue("Shell MinValue", range(0,1)) = 0.9
		_ShellMaxValue("Shell MaxValue", range(0,1)) = 0.9
		_FurLength("Fur Length", range(0,1)) = 1.0
		_ILayerAmount("Amount of layers", range(5, 32)) = 32
		_Forces("Gravity and wind", vector) = (0,0,0,0)
	}

	SubShader
	{
		//Tags{ "Rendertype" = "Transparent" }
		//Blend SrcAlpha OneMinusSrcAlpha
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 100
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			struct appdata
			{
				float4 pos : POSITION;
				float2 tex : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
			};

			sampler2D _DiffuseTex;
			float4 _DiffuseTex_ST;
			sampler2D _NoiseTex;

			v2f vert(appdata i)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(i.pos);
				o.tex = TRANSFORM_TEX(i.tex, _DiffuseTex);
				return o;
			}
			fixed4 frag(v2f i) : SV_Target0
			{
				fixed4 col = tex2D(_DiffuseTex, i.tex);
				return col;
			}
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct v2g
			{
				float4 pos : POSITION;
				float3 norm : NORMAL;
				float2 tex : TEXCOORD0;
			};

			struct g2f
			{
				float4 pos : SV_POSITION;
				float3 norm : NORMAL;
				float2 tex : TEXCOORD0;
				float lay : LAYER;
			};

			sampler2D _DiffuseTex;
			fixed4 _DiffuseTex_ST;
			sampler2D _NoiseTex;
			fixed4 _NoiseTex_ST;
			fixed _ShellMinValue;
			fixed _ShellMaxValue;
			fixed _FurLength;
			uint _ILayerAmount;
			fixed4 _Forces;

			void CreateVertex(inout TriangleStream<g2f> triStream, fixed4 vertexposition, fixed3 normal, fixed2 texCoord, fixed layerIndex)
			{
				g2f data;
				//data.pos = mul(float4(vertexposition.rgb, 1.0f), unity_ObjectToWorld);
				data.pos = UnityObjectToClipPos(vertexposition);

				data.norm = normalize(mul(fixed4(normal.xyz, 0), unity_ObjectToWorld));

				data.tex = texCoord;
				data.lay = layerIndex;
				triStream.Append(data);
			}

			v2g vert(v2g i)
			{
				v2g o;
				o.pos = i.pos;
				o.tex = TRANSFORM_TEX(i.tex, _DiffuseTex);
				o.norm = normalize(i.norm);
				return o;
			}

			[MaxVertexCount(96)]
			void geom(triangle v2g v[3], inout TriangleStream<g2f> triStream)
			{
				fixed _FLayerAmount = 1.0 / _ILayerAmount;

				fixed furLength0 = _FurLength;
				fixed furLength1 = _FurLength;
				fixed furLength2 = _FurLength;
				fixed offset0 = furLength0 * _FLayerAmount;
				fixed offset1 = furLength1 * _FLayerAmount;
				fixed offset2 = furLength2 * _FLayerAmount;

				int strongHair = _ILayerAmount / 4;
				for (uint i = 0; i < _ILayerAmount; ++i)
				{
					v[0].pos += fixed4(fixed3(v[0].norm), 1.0f) * offset0;
					v[1].pos += fixed4(fixed3(v[1].norm), 1.0f) * offset1;
					v[2].pos += fixed4(fixed3(v[2].norm), 1.0f) * offset2;

					int lay = i - strongHair;
					if (lay < 1)
					{
						lay = 0;
					}
					CreateVertex(triStream, v[0].pos + (_Forces * lay / _ILayerAmount), v[0].norm, v[0].tex, i + 1);
					CreateVertex(triStream, v[1].pos + (_Forces * lay / _ILayerAmount), v[1].norm, v[1].tex, i + 1);
					CreateVertex(triStream, v[2].pos + (_Forces * lay / _ILayerAmount), v[2].norm, v[2].tex, i + 1);

					triStream.RestartStrip();
				}
			}

			fixed4 frag(g2f i) : SV_Target0
			{
				fixed4 rgba = tex2D(_DiffuseTex, i.tex);
				fixed2 tex = TRANSFORM_TEX(i.tex, _NoiseTex);
				fixed shell = tex2D(_NoiseTex, tex).r;
				fixed alpha = _ShellMinValue + i.lay * ((_ShellMaxValue - _ShellMinValue) / _ILayerAmount);
				if (shell <= alpha)
				{
					discard;
				}
				rgba.a = alpha;
				return rgba;
			}
			ENDCG
		}
	}
}
