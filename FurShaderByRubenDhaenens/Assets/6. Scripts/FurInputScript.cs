﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurInputScript : MonoBehaviour {

    Renderer rend;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();

        rend.material.shader = Shader.Find("Fur/FurshaderV2.2");
	}
	
	// Update is called once per frame
	void Update () {
        float shininess = 1.0f;
        rend.material.SetFloat("_FurLength", shininess);
	}
}
